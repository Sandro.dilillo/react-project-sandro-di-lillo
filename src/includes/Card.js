import React, { useState } from 'react';


function Card({title, img, descrizione, location}) {
    
    const [titolo, setTitle ] = useState('prova')

    const cambiaTitolo = () => {
        setTitle("titolo cambiato");
    }
    // eslint-disable-next-line no-lone-blocks
   
    return (
        
        <section>
            
            <article className="card product-item">
                <header className="card__header">
                    <h1 className="product__location ">{location}</h1>
                    <h2>{titolo}</h2>
                </header>
                <div className="card__image">
                    <img src={img}
                        alt="" />
                </div>
                <div className="card__content">
                    <h2 className="product__title">{title}</h2>
                    <hr />
                    <p className="product__description">{descrizione}</p>
                </div>
                <div className="card__actions">
                    <button onClick={cambiaTitolo} className="btn tc-main">Details</button>
                </div>
            </article>
        </section>
    )
}

export default Card
