import React from 'react';



const Navigation = () => {
   
    return (
     
        <nav role="navigation" id="navbar-animmenu">
            <div >
                <ul className="show-dropdown main-navbar">
                    <div className="hori-selector"><div className="left"></div><div className="right"></div></div>
                    <li>
                        <a href="/#"><i className=""></i>Home</a>
                    </li>
                    <li className="active">
                        <a href="/#"><i className=""></i> I parchi</a>
                    </li>
                    <li>
                        <a href="/#"><i className=""></i>I giardini</a>
                    </li>
                    <li>
                        <a href="/#"><i className=""></i>I percorsi </a>
                    </li>
                    <li>
                        <a href="/#"><i className=""></i>I nostri concorsi</a>
                    </li>


                </ul>
            </div>
            

        </nav>
      
    )
}




export default Navigation
