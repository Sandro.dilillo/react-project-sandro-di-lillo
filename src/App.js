import Card from "./includes/Card"
import Navigation from "./includes/Navigation";
import products from "./products.js"


function App() {


  return (

    <div className="App">
      <section className="grid">
        <Navigation />
      </section>
      <section className="grid m-t-1">
        {products.map( (prodotto) => { 
          const { id } = prodotto; 
          return <Card 
                  key={ id } 
                  {...prodotto} 
                  />})}
    
      </section>



    </div>
  );
}

export default App;
